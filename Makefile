PWD ?= pwd_unknown
PROJECT_NAME = $(notdir $(PWD))
SERVICE_TARGET := main

ifeq ($(user),)
# USER retrieved from env, UID from shell.
HOST_USER ?= $(strip $(if $(USER),$(USER),nodummy))
HOST_UID ?= $(strip $(if $(shell id -u),$(shell id -u),4000))
else
# allow override by adding user= and/ or uid=  (lowercase!).
# uid= defaults to 0 if user= set (i.e. root).
HOST_USER = $(user)
HOST_UID = $(strip $(if $(uid),$(uid),0))
endif

THIS_FILE := $(lastword $(MAKEFILE_LIST))
CMD_ARGUMENTS ?= $(cmd)

export PROJECT_NAME
export HOST_USER
export HOST_UID

.PHONY: help package clean

help:
	@echo ''
	@echo 'Usage: make [TARGET] [EXTRA_ARGUMENTS]'
	@echo 'Targets:'
	@echo '  package  	    Generates HOT SPOT quarkus jar file.'
	@echo '  clean  	    Generated resources'
	@echo ''
	@echo 'Extra arguments:'
	@echo 'cmd=:	make cmd="whoami"'
	@echo '# user= and uid= allows to override current user. Might require additional privileges.'

package:
	docker run -v ${PWD}:/app/workdir dlucasfrancisco/ubuntu-quarkus-builder:19.10_11 /app/workdir/mvnw -Dmaven.test.skip=true package

clean:
	docker run -v ${PWD}:/app/workdir dlucasfrancisco/ubuntu-quarkus-builder:19.10_11 rm -rf /app/workdir/target